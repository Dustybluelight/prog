# list of imported modules
import requests
from bs4 import BeautifulSoup
import sqlite3

# variables
albumId = 39629
Id = 2
bandLimit = 11148
bandNumber = 7277
BandList = []
BandNameList = []
AlbumList = []

# database connections
conn = sqlite3.connect('MmmmChinook.db')
c = conn.cursor()

# function for inserting four column data
def InsertTable4(Table, Column1, Column2, Column3, Column4, Data1, Data2, Data3, Data4):
    c.execute("INSERT INTO {} ({}, {}, {}, {}) VALUES (?, ?, ?, ?)".format(Table, Column1, Column2, Column3, Column4),
              (Data1, Data2, Data3, Data4))
    conn.commit()


# function for deleting data from table
def DeleteData(Table, ConditionTable, Condition):
    sql = "DELETE FROM {} WHERE {} = {}".format(Table, ConditionTable, Condition)
    print(sql)
    c.execute(sql)
    conn.commit()


# function for scraping the band data on letter and adding to list
def ScrapeBanddata(StartingLetter):
    url = 'https://www.progarchives.com/bands-alpha.asp?letter=' + StartingLetter
    r = requests.get(url)
    s = BeautifulSoup(r.content, 'html.parser')
    for text in s.find_all('td'):
        b = text.get_text()
        b = b.strip()
        BandList.append(b)
    print(BandList)
    return BandList


def ScrapeBandAlbum(BandNumber):
    url = 'https://www.progarchives.com/artist.asp?id=' + str(BandNumber)
    r = requests.get(url)
    s = BeautifulSoup(r.content, 'html.parser')
    for text in s.find_all('td'):
        b = text.get_text()
        b = b.strip()
        AlbumList.append(b)
    return AlbumList


def GetBandName(BandNumber):
    url = 'https://www.progarchives.com/artist.asp?id=' + str(BandNumber)
    r = requests.get(url)
    s = BeautifulSoup(r.content, 'html.parser')
    for text in s.find_all('h1'):
        b = text.get_text()
        b = b.strip()
        BandNameList.append(b)
    print(BandNameList)
    print('check me')
    return BandNameList


# function for returning the band depending on range
def ReturnBand(Range):
    print(BandList[Range])
    return BandList[Range]


"""# deletes data in given table
for i in range(39519, 120000, 1):
    DeleteData('Album', 'albumId', i)


# really clever bit of code that gathers and inserts id, name, genre, and country correctly into band table
for alphabet in range(97, 123, 1):
    ScrapeBanddata(chr(alphabet))
    for i in range(3, (len(BandList)), 3):
        InsertTable4('Band', 'bandId', 'bandName', 'bandGenre', 'bandCountry', Id, ReturnBand(i),
                     ReturnBand(i + 1), ReturnBand(i + 2))
        Id += 1
    BandList = []


# to search and insert the albums and their dates
while bandNumber != bandLimit:
    try:
        for bandIdNumber in c.execute('SELECT bandID FROM Band WHERE bandName=?', GetBandName(bandNumber)):
            for album in ScrapeBandAlbum(bandNumber):
                album = album.strip()
                irrelevant, sep, albumpartitioned = album.partition('ratings')
                print(albumpartitioned)
                AlbumList = []
                for data in albumpartitioned.split():
                    AlbumList.append(data)
                AlbumList.append(' '.join(AlbumList[0:-1]))
                print(AlbumList)
                InsertTable4('Album', 'albumId', 'bandIdFK', 'albumName', 'albumDate', albumId, bandIdNumber[0],
                             AlbumList[-1], AlbumList[-2])
                albumId += 1
                AlbumList = []
                BandNameList = []
    except:
        AlbumList = []
        BandNameList = []
    bandNumber += 1 


for bandIdNumber in c.execute('SELECT bandID FROM Band WHERE bandName=?', GetBandName(4429)):
    for album in ScrapeBandAlbum(4429):
        album = album.strip()
        irrelevant, sep, albumpartitioned = album.partition('ratings')
        print(albumpartitioned)
        AlbumList = []
        for data in albumpartitioned.split():
            AlbumList.append(data)
        AlbumList.append(' '.join(AlbumList[0:-1]))
        print(AlbumList)
        InsertTable4('Album', 'albumId', 'bandIdFK', 'albumName', 'albumDate', albumId, bandIdNumber[0],
                     AlbumList[-1], AlbumList[-2])
        albumId += 1
        AlbumList = []
        BandNameList = []
"""""
