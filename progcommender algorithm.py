import sqlite3
import random
import time
import sys


conn = sqlite3.connect('MmmmChinook.db')
c = conn.cursor()

def main():
    albumdateid = []
    ralbumId = None
    rbandId = None
    genrebandid = []
    countrybandid = []
    musicrelatability = {}
    top5albums = []
    top5ratings = []

    # assigning and checking variables for the album
    while rbandId is None or ralbumId is None:
        user = (input('Name an album you enjoy and the corresponding band in the format "Album % Band". ')).upper()
        same = (input("Type 'y' if would prefer to have more different band's material recommended to you. "))
        album, sep, band = user.partition(' % ')
        rbandcountry = grabandassignfromdb('bandCountry', 'Band', 'bandName', band, 0, 0, 1, 'v')
        rbandId = grabandassignfromdb('bandId', 'Band', 'bandName', band, 0, 0, 1, 'v')
        genre = grabandassignfromdb('bandGenre', 'Band', 'bandId', rbandId, 0, 0, 1, 'v')
        ralbumId = grabandassignfromdb('albumId', 'Album', 'albumName', album, 'bandIdFK', rbandId, 2, 'v')
        if ralbumId is None or rbandId is None:
            print("Sorry we don't have that band, please try another one.")
    date = grabandassignfromdb('albumDate', 'Album', 'AlbumId', ralbumId, 0, 0, 1, 'v')
    for row in c.execute('SELECT albumDate, albumId FROM Album'):
        albumdateid.append(row)
    for t in albumdateid:
        x = t[0] - date
        if -3 <= x <= 3:
            y = (7 / 720) * (x ** 6) + (1 / 48) * (x ** 5) - (13 / 144) * (x ** 4) - (17 / 48) * (x ** 3) - (
                        151 / 360) * (x ** 2) + (11 / 6) * x + 9
            musicrelatability[t[1]] = y
        else:
            musicrelatability[t[1]] = 0
    musicrelatability[ralbumId] -= 100
    # code to add same genre points and country
    for row in c.execute('SELECT bandId FROM Band WHERE bandGenre =?', (genre,)):
        genrebandid.append(row[0])
    for row in c.execute('SELECT bandId FROM Band WHERE bandCountry =?', (rbandcountry,)):
        countrybandid.append(row[0])
    for row in c.execute('SELECT albumId, bandIdFK FROM Album'):
        if row[1] in genrebandid:
            musicrelatability[row[0]] += 8
        if row[0] in countrybandid:
            musicrelatability[row[0]] += 4
    # code to add same band points
    for row in c.execute('SELECT albumId FROM Album WHERE bandIdFk =?', (rbandId,)):
        if same is 'y':
            musicrelatability[row[0]] += -40
        else:
            musicrelatability[row[0]] += 4

    musicrelatability = {k: v for k, v in sorted(musicrelatability.items(), key=lambda x: x[1])}
    for i in range(-1, -7, -1):
        top5albums.append(list(musicrelatability)[i])
    print(' ')
    print('The top 5 most similar albums are:')
    for i in range(0, 6, 1):
        top5ratings.append(musicrelatability.get(top5albums[i]))
        for album in c.execute('SELECT albumName, bandIdFK, albumDate FROM Album WHERE albumId =?', (top5albums[i],)):
            for row in c.execute('SELECT bandName FROM Band WHERE bandId =?', (album[1],)):
                bandnames = row[0]
            if same is 'n':
                upper = 5
            else:
                upper = -0.36
            n = 5
            while n != round((100 * top5ratings[i] / 26 + 1)):
                sys.stdout.write('\r{} by {} with a similarity of {}%'.format(album[0], bandnames, str(
                    round(n + random.uniform(-5, upper), 2))))
                sys.stdout.flush()
                time.sleep(0.008)
                n += 0.5
            sys.stdout.write('\n{} by {} with a similarity of {}%'.format(album[0], bandnames,
                                                                          str(round(n + random.uniform(-5, upper), 2))))
            sys.stdout.write('\rCalculations Complete')

            sys.stdout.flush()

def grabandassignfromdb(select, table, conditioncoloumn, condition, oconditioncoloumn, ocondition, amountofconditions, typeofreturn):
    if amountofconditions == 1:
        if typeofreturn is 'v':
            for row in c.execute('SELECT {} FROM {} WHERE UPPER({}) =UPPER(?)'.format(select, table, conditioncoloumn), (condition,)):
                return row[0]
        else:
            for row in c.execute('SELECT {}, {} FROM {}'.format(select, condition, table)):
                return row

    elif amountofconditions == 2:
        for row in c.execute('SELECT {} FROM {} WHERE UPPER({}) =UPPER(?) AND {} =?'.format(select, table, conditioncoloumn, oconditioncoloumn), (condition, ocondition)):
            return row[0]

repeat = True
while repeat == True:
    main()
    print(' ')
    repeat = input('\nWould you like to pick another album to compare? ')
    if repeat.upper() == 'YES' or repeat.upper() == 'Y':
        repeat = True



